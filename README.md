# Nginx + 靜態網站 + GCE

## 方法一 (手動無極限)

1. 建立VM

   ```bash
   gcloud compute instances create <vm_name> --machine-type n1-standard-2 --tags http-server --zone [your_zone]
   ```

2. SSH進去到VM

   ```bash
   gcloud compute ssh <vm_name> --zone [YOUR_ZONE]
   ```

   + 安裝Nginx&Git

     ```bash
     # 取得root權限
     sudo su -
     # 更新OS
     apt-get update
     # 安裝Nginx
     apt-get install -y nginx
     # 安裝Git
     apt-get install -y git
     ```

   + 刪除Nginx預設的頁面

     ```bash
     rm -f /etc/nginx/sites-enabled/default
     ```

   + Clone靜態網站專案

     ```bash
     git clone https://gitlab.com/KennyChenFight/nginx-html-demo.git
     ```

   + 複製專案內的Nginx設定檔、相關html檔案

     ```bash
     cp ./nginx-html-demo/nginx.conf /etc/nginx/conf.d
     cp ./nginx-html-demo/demo.html /var/www/html
     ```

   + Nginx重新reload配置

     ```bash
     /etc/init.d/nginx reload
     ```

3. 開啟VM的External IP 即可看到~

## 方法二 (少數手動+Docker)

1. 建立VM

   ```bash
   gcloud compute instances create <vm_name> --machine-type n1-standard-2 --tags http-server --zone [your_zone]
   ```

2. SSH進去到VM

   ```bash
   gcloud compute ssh <vm_name> --zone [YOUR_ZONE]
   ```

   + 安裝Docker

     ```bash
     # 取得root權限
     sudo su -
     # 安裝Docker
     curl -fsSL https://get.docker.com -o get-docker.sh
     sudo sh get-docker.sh
     sudo usermod -aG docker <username>
     ```

   + Clone靜態網站專案

     ```bash
     git clone https://gitlab.com/KennyChenFight/nginx-html-demo.git
     ```

   + docker build & run

     ```bash
     cd nginx-html-demo/
     docker build . -t nginx-html-demo
     docker run --name nginx-container -p 80:80 -d nginx-html-demo
     ```

3. 開啟VM的External IP 即可看到~

## 方法三 (加入start-script)

1. local 啟動start-script

   1. 本地Clone靜態網站專案

      ```bash
      git clone https://gitlab.com/KennyChenFight/nginx-html-demo.git
      ```

   2. 建立VM+啟動start-script

      ```bash
      gcloud compute instances <vm_name> --metadata-from-file startup-script=./nginx-html-demo/start-script-1.sh --tags http-server --zone [your_zone]
      ```
   3. 要等一段時間在開External IP~

   

## 方法四 (直接使用GCE+GCR)

1. 本地Clone靜態網站專案

   ```bash
   git clone https://gitlab.com/KennyChenFight/nginx-html-demo.git
   ```

2. 本地docker build 

   ```bash
   cd nginx-html-demo
   docker build . -t nginx-html-demo
   ```

3. tag docker image

   ```bash
   docker tag nginx-html-demo gcr.io/<project_id>/nginx-html-demo:v1
   ```

4. push image to gcr

   ```bash
   docker push gcr.io/<project_id>/nginx-html-demo:v1
   ```

5. 建立VM並指定container

   ```bash
   gcloud compute instances create-with-container <vm_name> --container-image gcr.io/<project_id>/nginx-html-demo:v1 --tags http-server --zone [your_zone]
   ```

6. 開啟VM的External IP 即可看到~

### 如何更新VM上的容器?

先push到gcr上，接著利用指令更新

```bash
gcloud compute instances update-container <vm_name> \
    --container-image gcr.io/<project_id>/nginx-html-demo:<version>
```



