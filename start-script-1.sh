#! /bin/bash
sudo su -
# 更新OS
apt-get update
# 安裝Nginx
apt-get install -y nginx
# 安裝Git
apt-get install -y git
rm -f /etc/nginx/sites-enabled/default
# deploy
git clone https://gitlab.com/KennyChenFight/nginx-html-demo.git
cp ./nginx-html-demo/nginx-1.conf /etc/nginx/conf.d
cp ./nginx-html-demo/demo.html /var/www/html
/etc/init.d/nginx reload
